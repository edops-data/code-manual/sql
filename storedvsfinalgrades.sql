WITH Stored AS (
  SELECT StudentID,
    Course_Number,
    SectionID,
    TermID,
    StoreCode,
    Percent,
    Grade
  FROM StoredGrades
),
Live AS (
  SELECT pgf.StudentID,
    c.Course_Number,
    pgf.SectionID,
    sec.TermID,
    pgf.FinalGradeName AS StoreCode,
    pgf.Percent,
    pgf.Grade
  FROM PGFinalGrades pgf
  LEFT JOIN Sections sec ON sec.ID = pgf.SectionID
  LEFT JOIN Courses c ON c.Course_Number = sec.Course_Number
)
SELECT s.Student_Number,
  s.LastFirst,
  s.Grade_Level,
  cc.Course_Number,
  cc.SectionID,
  lg.Percent AS PercentLive,
  lg.Grade AS GradeLive,
  sg.Percent AS PercentStored,
  sg.Grade AS GradeStored,
  (sg.Percent - lg.Percent) AS PercentDelta,
  CASE
    WHEN lg.Grade <> sg.Grade THEN 'Y'
    ELSE NULL
  END AS GradeDiff
FROM CC
LEFT JOIN Students s ON s.ID = cc.StudentID
LEFT JOIN Live lg ON lg.StudentID = cc.StudentID
  AND lg.Course_Number = cc.Course_Number
  AND lg.SectionID = cc.SectionID
  AND lg.TermID = cc.TermID
LEFT JOIN Stored sg ON sg.StudentID = cc.StudentID
  AND sg.Course_Number = cc.Course_Number
  AND sg.SectionID = cc.SectionID
WHERE cc.TermID LIKE '28%'
  AND lg.StoreCode = 'Q1'
  AND sg.StoreCode = 'Q1'
