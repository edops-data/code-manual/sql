# Git II - The SQL

## Part 0: Preparation

### A. Install Git

If you haven't already.

Navigate to [https://git-scm.com/downloads](https://git-scm.com/downloads), and install Git.  Leave all options as their defaults.

### B. Install Atom

Any text editor will be fine, since source code is stored as plain text.  However, there are two popular code editors with built-in support for Git. Built-in support = lazy user = happy user.

Navigate to [https://atom.io](https://atom.io), download, and install.

## Part 1: Clone your school's Git repository

Navigate to [https://gitlab.com/edops-data](https://gitlab.com/edops-data) and locate your school.  Copy the URL into the clipboard.
![](./images/copyurl.PNG)

Go to your desktop, right-click, and choose **Git Bash here**.
![](./images/gitbashhere.PNG)

Type `git clone ` (including the space) and then right-click to paste the URL you copied.
> It should look like `git clone https://gitlab.com/edops-data/Paul-PCS`.  Press **Enter**.
![](./images/gitclone.PNG)

Watch the text whiz by.

Congrats!  You just cloned your git repository to the desktop.  Type `exit`, press **Enter**, and forget about all that command line nonsense.
![](./images/clonesuccess.PNG)

## Part 2: Load your repo into Atom

In Atom, click **File | Add Project Folder** and navigate to the git repo you just downloaded to the desktop.  You should now see the folder structure under the _Project_ heading.

## Part 3: Get into APEX

Open the PS Admin portal of your favorite school.

Make a new tab, and navigate to `https://[your PS Instance]:8443/ords`.  For example, `https://paulpcs.powerschool.com:8443/ords`
![](./images/apexlogin.PNG).  Sign in using your APEX credentials, which are different from your PS credentials.

Click on **SQL Workshop**, and then on **SQL Commands**.

## Part 4: Build a query

### Stored Grades

This first query pulls all of the stored grades for everybody, ever.  (That's fine; we'll pare it down later.)

```sql
SELECT StudentID,
  Course_Number,
  SectionID,
  TermID,
  StoreCode,
  Percent,
  Grade
FROM StoredGrades
```

### Live Grades

This second query pulls all of the live grades for everybody, ever.

```sql
SELECT pgf.StudentID,
  c.Course_Number,
  pgf.SectionID,
  sec.TermID,
  pgf.FinalGradeName AS StoreCode,
  pgf.Percent,
  pgf.Grade
FROM PGFinalGrades pgf
LEFT JOIN Sections sec ON sec.ID = pgf.SectionID
LEFT JOIN Courses c ON c.Course_Number = sec.Course_Number
```

### Using CTEs

To use these queries together, we're going to give each one a name.

```sql
WITH Stored AS (
  SELECT StudentID,
    Course_Number,
    SectionID,
    TermID,
    StoreCode,
    Percent,
    Grade
  FROM StoredGrades
),
Live AS (
  SELECT pgf.StudentID,
    c.Course_Number,
    pgf.SectionID,
    sec.TermID,
    pgf.FinalGradeName AS StoreCode,
    pgf.Percent,
    pgf.Grade
  FROM PGFinalGrades pgf
  LEFT JOIN Sections sec ON sec.ID = pgf.SectionID
  LEFT JOIN Courses c ON c.Course_Number = sec.Course_Number
)
```

> From here on out, in order to run you'll have to hit **CTRL-A** to select all, then **CTRL-Enter** to run.

### Joining Everything Together

Now we can join these two queries together.  They're still going to return every grade for every student ever, though.

Add the following to the _end_ of your query.

```sql
SELECT s.Student_Number,
  s.LastFirst,
  s.Grade_Level,
  cc.Course_Number,
  cc.SectionID,
  lg.Percent AS PercentLive,
  lg.Grade AS GradeLive,
  sg.Percent AS PercentStored,
  sg.Grade AS GradeStored,
  (sg.Percent - lg.Percent) AS PercentDelta,
  CASE
    WHEN lg.Grade <> sg.Grade THEN 'Y'
    ELSE NULL
  END AS GradeDiff
FROM CC
LEFT JOIN Students s ON s.ID = cc.StudentID
LEFT JOIN Live lg ON lg.StudentID = cc.StudentID
  AND lg.Course_Number = cc.Course_Number
  AND lg.SectionID = cc.SectionID
  AND lg.TermID = cc.TermID
LEFT JOIN Stored sg ON sg.StudentID = cc.StudentID
  AND sg.Course_Number = cc.Course_Number
  AND sg.SectionID = cc.SectionID
```

### Restricting the Results

Let's finally add some restrictions to the query so that it only pulls from SY2019, Quarter 1.

Add the following to the _end_ of your query.

```sql
WHERE cc.TermID LIKE '28%'
  AND lg.StoreCode = 'Q1'
  AND sg.StoreCode = 'Q1'
```

### Success!

![](./images/partyparrot.gif)

If anything fell apart, you can get the full code at [./storedvsfinalgrades.sql](./storedvsfinalgrades.sql)

Save your file under `SQLReports` and commit to Git:

1.  Make sure that the **Git** panel (not Github) is selected on the right side of Atom.
2.  Click **Stage All** to stage your new file.
3.  Under **Commit Message** type `SQL Report: Stored vs. Live Grades`
4.  Click **Commit to master**.
5.  Click **Push**.

## Part 4: Make a SQL Report

Return to the PS Admin portal.

### Enter the SQL Query

Navigate to **SQLReports**.  At the bottom of the screen, click **Create a new SQLReport**.

Name your report `Stored vs. Live Grades`

Under **SQL Query**, paste the query you just wrote, with **3 important alterations**:

-   Change `cc.TermID LIKE '28%'` to read `cc.TermID LIKE '~(curyearid)%'`
-   Change `AND lg.StoreCode = 'Q1'` to read `AND lg.StoreCode = '%param1%'`
-   Change `AND sg.StoreCode = 'Q1'` to read `AND sg.StoreCode = '%param1%'`

> This enables the user to choose which year and term they are reporting on.

Click **Build Headings** and name the resultant headings as follows:

-   Student ID
-   Student name
-   Grade Level
-   Course
-   Section ID
-   Live Percent
-   Live Grade
-   Stored Percent
-   Stored Grade
-   Delta
-   Letter Change

![](./images/reportdetails.PNG)

Click **Submit** to save the report.

### Enter the parameter info

Click the pencil icon next to your report so you can make some more edits.

Click on the **Parameters** tab.

Under **Parameter 1** enter `Term` for _Name_, under _Use Drop Down_ select `Terms - Grading`.

![](./images/parameter.PNG)

Click **Submit**.

### Success!

![](./images/partyparrot.gif)
